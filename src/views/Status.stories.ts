import {
  mockStorybookApi,
  overrideMockStorybookHandlers
} from "@/utils/storybook/mock-api-storybook";
import StatusPage from "./Status.vue";
import { StoryFnVueReturnType } from "@storybook/vue/dist/client/preview/types";
import { VueStory } from "@/utils/storybook/types";
import {
  mockSimpleStatus,
  mockStatusWithFullUpdateAbort,
  mockStatusWithFullUpdateDone,
  mockStatusWithFullUpdateFailure,
  mockStatusWithFullUpdatePending
} from "@/api-client/backend-mocks/status";
import { action } from "@storybook/addon-actions";
import { mockUpdateAllMetadata } from "@/api-client/backend-mocks/metadata";

export default {
  title: "Status",
  decorators: [
    mockStorybookApi(
      mockSimpleStatus(),
      mockUpdateAllMetadata(action("fullUpdate"))
    )
  ]
};

export const Default = (): StoryFnVueReturnType => {
  return {
    components: { StatusPage },
    template: "<StatusPage/>"
  };
};

export const WithFullUpdate: VueStory = Default.bind({});
WithFullUpdate.decorators = [
  overrideMockStorybookHandlers(mockStatusWithFullUpdatePending())
];

export const WithAbortedFullUpdate: VueStory = Default.bind({});
WithAbortedFullUpdate.decorators = [
  overrideMockStorybookHandlers(mockStatusWithFullUpdateAbort())
];

export const WithSuccessfulFullUpdate: VueStory = Default.bind({});
WithSuccessfulFullUpdate.decorators = [
  overrideMockStorybookHandlers(mockStatusWithFullUpdateDone())
];

export const WithErrornousFullUpdate: VueStory = Default.bind({});
WithErrornousFullUpdate.decorators = [
  overrideMockStorybookHandlers(mockStatusWithFullUpdateFailure())
];
