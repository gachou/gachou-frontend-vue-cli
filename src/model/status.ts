export interface Status {
  tasks: {
    running?: number;
    waiting?: number;
  };
  fullUpdateStatus?: {
    lastStarted?: string; // date-time
    lastFinished?: string; // date-time
    abortRequested?: boolean;
    messages: string[];
    fatalError?: string;
    totalFiles: number;
    stoppedBecause?: StoppedBecause;
    done: number;
  };
}

export type StoppedBecause = "done" | "aborted" | "error";
