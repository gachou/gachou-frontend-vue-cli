import { StoryFnVueReturnType } from "@storybook/vue/dist/client/preview/types";
import DashboardValue from "@/components/DashboardValue.vue";

export default {
  title: "components/DashboardValue"
};

export const Default = (): StoryFnVueReturnType => {
  return {
    components: { DashboardValue },
    template: "<DashboardValue label='SomeLabel' value='12230'/>"
  };
};
