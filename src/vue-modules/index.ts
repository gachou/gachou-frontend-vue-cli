import "../styles/main.scss";
import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueCompositionAPI from "@vue/composition-api";

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.use(VueCompositionAPI);
