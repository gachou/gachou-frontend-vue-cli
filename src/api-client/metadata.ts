import { gachouOpenapiClient } from "@/api-client/openapi/gachou-openapi-client";

export async function updateAllMetadata() {
  return gachouOpenapiClient.UpdateAllMetadata();
}
