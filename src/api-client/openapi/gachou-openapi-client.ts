import { Client } from "./generated/client";
import openApiSpec from "./generated/gachou-openapi.json";
import { OpenAPIClientAxios, Document } from "openapi-client-axios";

const api = new OpenAPIClientAxios({ definition: openApiSpec as Document });

export const gachouOpenapiClient = api.initSync<Client>();
