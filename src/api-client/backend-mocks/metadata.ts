import { RequestHandlersList } from "msw/lib/types/setupWorker/glossary";
import { rest } from "msw";
import { Paths } from "@/api-client/openapi/generated/client";

export function mockUpdateAllMetadata(
  callback: () => Promise<void> | void
): RequestHandlersList[number] {
  return rest.put(
    "http://localhost:5000/metadata",
    async (req, res, context) => {
      await callback();
      const responseBody: Paths.UpdateAllMetadata.Responses.$200 = {
        success: "true"
      };
      return res(context.json(responseBody));
    }
  );
}
