import { MockedRequest, rest } from "msw";
import { RequestHandlersList } from "msw/lib/types/setupWorker/glossary";
import { Components } from "@/api-client/openapi/generated/client";
import Status = Components.Schemas.Status;

function mockStatusEndpoint(
  computeResponse: (req: MockedRequest) => Status
): RequestHandlersList[number] {
  return rest.get("http://localhost:5000/status", (req, res, context) => {
    return res(context.json(computeResponse(req)));
  });
}

export function mockSimpleStatus(): RequestHandlersList[number] {
  return mockStatusEndpoint(() => ({
    tasks: {
      running: 31415,
      waiting: 31416
    }
  }));
}

export function mockStatusWithFullUpdatePending(): RequestHandlersList[number] {
  return mockStatusEndpoint(() => ({
    tasks: {
      running: 31415,
      waiting: 31416
    },
    fullUpdateStatus: {
      totalFiles: 512,
      done: 480,
      messages: [
        "Message 1 and a little bit longer...",
        "Message 2 and a little bit longer...",
        "Message 3 and a little bit longer..."
      ],
      lastStarted: "2017-02-03T10:11:12+01:00",
      lastFinished: "2017-02-03T10:11:12+02:00"
    }
  }));
}

export function mockStatusWithFullUpdateFailure(): RequestHandlersList[number] {
  return mockStatusEndpoint(() => ({
    tasks: {
      running: 31415,
      waiting: 31416
    },
    fullUpdateStatus: {
      totalFiles: 512,
      done: 480,
      messages: [
        "Message 1 and a little bit longer...",
        "Message 2 and a little bit longer...",
        "Message 3 and a little bit longer..."
      ],
      lastStarted: "2017-02-03T10:11:12+01:00",
      lastFinished: "2017-02-03T10:11:12+02:00",
      stoppedBecause: "error",
      fatalError: "This was the fatal error that cause the progress to stop."
    }
  }));
}

export function mockStatusWithFullUpdateAbort(): RequestHandlersList[number] {
  return mockStatusEndpoint(() => ({
    tasks: {
      running: 31415,
      waiting: 31416
    },
    fullUpdateStatus: {
      totalFiles: 512,
      done: 480,
      messages: [
        "Message 1 and a little bit longer...",
        "Message 2 and a little bit longer...",
        "Message 3 and a little bit longer..."
      ],
      lastStarted: "2017-02-03T10:11:12+01:00",
      lastFinished: "2017-02-03T10:11:12+02:00",
      stoppedBecause: "aborted"
    }
  }));
}

export function mockStatusWithFullUpdateDone(): RequestHandlersList[number] {
  return mockStatusEndpoint(() => ({
    tasks: {
      running: 31415,
      waiting: 31416
    },
    fullUpdateStatus: {
      totalFiles: 512,
      done: 512,
      messages: [
        "Message 1 and a little bit longer...",
        "Message 2 and a little bit longer...",
        "Message 3 and a little bit longer..."
      ],
      lastStarted: "2017-02-03T10:11:12+01:00",
      lastFinished: "2017-02-03T10:11:12+02:00",
      stoppedBecause: "done"
    }
  }));
}
