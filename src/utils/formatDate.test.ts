import { formatDate } from "./formatDate";

test("returns a local version of the given date", () => {
  expect(formatDate("2017-02-03T10:11:12+02:00")).toEqual("");
});
