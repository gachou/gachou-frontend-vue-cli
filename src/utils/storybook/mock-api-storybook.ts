import { RequestHandlersList } from "msw/lib/types/setupWorker/glossary";
import { DecoratorFunction } from "@storybook/addons";
import { setupWorker } from "msw";

const worker = setupWorker();
worker.start().catch(console.error);

export function mockStorybookApi<T>(
  ...requestHandlers: RequestHandlersList
): DecoratorFunction<T> {
  return _storyFn => {
    worker.resetHandlers(...requestHandlers);
    console.log("Resetting mocks, to");
    worker.printHandlers();
    return _storyFn();
  };
}

export function overrideMockStorybookHandlers<T>(
  ...requestHandlers: RequestHandlersList
): DecoratorFunction<T> {
  return _storyFn => {
    console.log("Adjusting mocks");
    worker.use(...requestHandlers);
    worker.printHandlers();
    return _storyFn();
  };
}
