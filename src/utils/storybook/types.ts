import { StoryFnVueReturnType } from "@storybook/vue/dist/client/preview/types";
import { Story } from "@storybook/vue";

export type VueStory = Story<StoryFnVueReturnType>;
