import { rest } from "msw";
import { setupApiMock } from "./mock-api";
import { gachouOpenapiClient } from "@/api-client/openapi/gachou-openapi-client";
import { Components } from "@/api-client/openapi/generated/client";
import Status = Components.Schemas.Status;

describe("setupApiMock", () => {
  setupApiMock(
    rest.get("http://localhost:5000/status", (_req, res, context) => {
      return res(
        context.json({
          tasks: {
            running: 31415,
            waiting: 31416
          }
        } as Status)
      );
    })
  );
  it("makes the api client return the mocked response", async () => {
    const status = await gachouOpenapiClient.GetStatus();
    expect(status.data).toEqual({ tasks: { running: 31415, waiting: 31416 } });
  });
});
