import { setupServer } from "msw/node";
import { RequestHandlersList } from "msw/lib/types/setupWorker/glossary";
import { MockedRequest, ResponseComposition, ResponseTransformer } from "msw";

interface Result {
  interceptedRequests: () => MockedRequest[];
}

function augmentedResponse(
  res: ResponseComposition,
  additionalTransformers: ResponseTransformer[]
): ResponseComposition {
  const customResponseComposition: typeof res = (...transformers) => {
    return res(...transformers, ...additionalTransformers);
  };
  customResponseComposition.once = (...transformers) => {
    return res.once(...transformers, ...additionalTransformers);
  };
  customResponseComposition.networkError = message => res.networkError(message);
  return customResponseComposition;
}

export function setupApiMock(...restHandlers: RequestHandlersList): Result {
  let requests: MockedRequest[] = [];

  const observingHandlers: RequestHandlersList = restHandlers.map(handler => {
    return {
      ...handler,
      resolver: (req, res, context) => {
        requests.push(req);
        const customResponseComposition = augmentedResponse(res, [
          // context.set("Access-Control-Allow-Origin:", "*"),
        ]);

        return handler.resolver(req, customResponseComposition, context);
      }
    };
  });

  const server = setupServer(...observingHandlers);

  beforeEach(async () => {
    requests = [];
    await server.listen();
  });

  afterEach(async () => {
    if (server != null) {
      await server.close();
    }
  });

  return {
    interceptedRequests() {
      return requests;
    }
  };
}
