import dayjs from "dayjs";
import "dayjs/locale/de";
import localizedFormat from "dayjs/plugin/localizedFormat";
import relativeTime from "dayjs/plugin/relativeTime";

dayjs.extend(localizedFormat);
dayjs.extend(relativeTime);
dayjs.locale("de");

export function formatDate(isoDate: string): string {
  const date = new Date(isoDate);
  return dayjs(date).format("L LT");
}

export function relativeTimeFromNow(isoDate: string): string {
  const date = new Date(isoDate);
  return dayjs(date).fromNow();
}
