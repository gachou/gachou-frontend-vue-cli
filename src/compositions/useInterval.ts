import { onBeforeUnmount, onMounted, ref } from "@vue/composition-api";

type IntervalHandle = ReturnType<typeof setInterval>;

export function useInterval(callback: () => {}, intervalMillis: number) {
  const updateTimeout = ref<IntervalHandle | null>(null);
  onMounted(() => {
    updateTimeout.value = setInterval(callback, intervalMillis);
    callback();
  });
  onBeforeUnmount(() => {
    if (updateTimeout.value != null) {
      clearInterval(updateTimeout.value);
    }
  });
}
