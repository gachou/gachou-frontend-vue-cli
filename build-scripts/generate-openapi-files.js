const fs = require('fs');
const yaml = require('js-yaml');
const path = require('path');
const os = require('os')
const prettier = require('prettier');
const { generateTypesForDocument } = require('openapi-client-axios-typegen');
const jsonRefs = require('json-refs')

const openapiYamlFile = require.resolve(
  '../src/api-client/openapi/gachou-openapi.yaml'
);
const targetDir = path.join(path.dirname(openapiYamlFile), 'generated');
const clientDts = path.join(targetDir, 'client.d.ts');
const openApiJsonFile = path.join(targetDir, 'gachou-openapi.json');

function writeAsJson(openapiSpec) {
  const json = JSON.stringify(openapiSpec);
  fs.writeFileSync(openApiJsonFile, prettier.format(json, { parser: 'json' }));
}

async function writeTypes(openapiSpec) {
  const tmpFile = path.join(os.tmpdir(), `gachou-openapi.resolved.${Date.now().toString(36)}.json`)
  const resolvedSpec = await jsonRefs.resolveRefs(openapiSpec, {
     filter: ref => !ref.uri.startsWith('#/components/schemas')
  })
  fs.writeFileSync(tmpFile, JSON.stringify(resolvedSpec.resolved))
  const [
    imports,
    schemaTypes,
    operationTypings,
  ] = await generateTypesForDocument(tmpFile, { transformOperationName: operation => operation});
  const dtsContents = '/* eslint-disable */\n'+imports + '\n' + schemaTypes + '\n ' + operationTypings;
  fs.writeFileSync(clientDts, dtsContents);
}

try {
  const openapiSpec = yaml.safeLoad(fs.readFileSync(openapiYamlFile, 'utf8'));
  writeAsJson(openapiSpec);
  writeTypes(openapiSpec).catch(console.error)
} catch (e) {
  console.error(e);
}
